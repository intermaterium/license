package License::License;

use Path::Tiny;
use DateTime qw();
use File::Basename;
use File::Spec;

# Get the absolute path to this file for reading the resourses
my $licenseDir = dirname(File::Spec->rel2abs(__FILE__));

=begin comment
Class constructor

@param string $name
    The name of the license desired by the client
@dies
    If the license is not known by the program
=cut
sub new
{
    my ($class, $name) = @_;
    my $self = {
        __name => $name
    };

    bless $self, $class;

    die("License $name is not a known license\n") if !$self->licenseExists;

    return $self;
}

=begin comment
Checks if the user provided license is known by the program
@returns bool
    If the license is known or not
=cut
sub licenseExists
{
    my $self = shift;
    my $name = uc $self->{__name};
    my $path = "$licenseDir/licenses/$name.md";
    return (-f $path);
}

=begin comment
Retrieves the content of the license file.

If the user provided a author name or one is stored in configs and the
licenses allows it, the text will be contain the authors name

If the license has a section for the year, that will also be filled

@returns string
    The content of the file, possibly modifed
=cut
sub getLicenseContent
{
    my ($self, $config) = @_;
    my $name = uc $self->{__name};
    my $path = "$licenseDir/licenses/$name.md";
    my $content = path($path)->slurp;

    if (defined($config->{__author}) && index($content, '<author>') != -1) {
        my $author = $config->{__author};
        $content =~ s/<author>/$author/;
    }

    if (index($content, '<year>') != -1) {
        my $year = DateTime->now->strftime('%Y');
        if (defined($config->{__year})) {
            $year = $config->{__year};
        }
        $content =~ s/<year>/$year/;
    }

    return $content;
}

sub listLicenses
{
    my $self = shift;
    my $iter = path("$licenseDir/licenses")->iterator;
    while (my $path = $iter->()) {
        $path =~ /.*\/(.*)\.md/;
        print "$1\n";
    }
}

1;
