package License::Config;

use Path::Tiny;
use JSON;
use File::Basename;

sub new
{
    my $class = shift;
    my $self = {
        __path => shift
    };
    bless $self, $class;
    return $self;
}

sub setAuthor
{
    my ($self, $author) = @_;
    unless(!defined($author)) {
        $self->{__author} = $author;
    }
}

sub setYear
{
    my ($self, $year) = @_;
    unless(!defined($year)) {
        $self->{__year} = $year;
    }
}


=begin comment
Creates a user config file if it does not exist
=cut
sub createConfigFile
{
    my $self = shift;
    my $path = $self->{__path};
    unless (-f $path) {
        path(dirname($path))->mkpath;
        path($path)->spew("{}");
    }
}

=begin comment
Reads any previously stored user configs and stores into the object
=cut
sub readConfigFile
{
    my $self = shift;
    $self->createConfigFile;
    my $content = path($self->{__path})->slurp;
    my $decoded = decode_json($content);

    if (defined($decoded->{'author'})) {
        $self->{__author} = $decoded->{'author'};
    }
}

=begin comment
Saves the configs stored in the object to file in the config directory
=cut
sub saveConfigs
{
    my $self = shift;
    my %config = ();

    if (defined($self->{__author})) {
        $config{'author'} = $self->{__author};
    }

    my $json = encode_json(\%config);

    path($self->{__path})->spew($json);
}

=begin comment
Purges any stored configs we might have for the user
=cut
sub purgeConfigs
{
    my $self = shift;
    my $path = $self->{__path};
    if (-f $path) {
        path($path)->spew("{}");
    }
}

1;
