# NAME

License - A simple license generation program

# SYNOPSIS

Usage: license \[command|license\] \[options\]

ie license MIT --author "Intermaterium" -o LICENSE

# DESCRIPTION

A program to generate licenses for use with projects. 

# COMMANDS

- **list**

        Lists all currently known licenses.

- **config**

        Configure stored details for the program.

- **license**

        The license you will like to use. If the license is not known the program
        will exit with a non-zero error code.

# OPTIONS

- **-h --help**

        Prints this help.

- **-v --version**

        Prints the program version.

- **-o --output**

        File to output the license to.

- **-a --author**

        The author or list of authors to be attributed on the license. If used with
        the config command it will set the default user to use for licenses when one
        is not provided. Only modifies the license if the license allows it.

- **-y --year**

        The year or list of years for the license ie 2018. If not provided will
        default to the current year. Only modifies the license if the license allows
        it.

- **-p --purge**

        Purge all stored configs

# LICENSE

This is released under the Unlicense. 
See [https://unlicense.org/](https://unlicense.org/).

# AUTHOR

Intermaterium - [chris.birmingham@hotmail.co.uk](https://metacpan.org/pod/chris.birmingham@hotmail.co.uk)
