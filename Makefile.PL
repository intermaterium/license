use strict;
use warnings;

use ExtUtils::MakeMaker;

WriteMakefile
(
    NAME => 'License',
    AUTHOR => 'Intermaterium <chris.birmingham@hotmail.co.uk>',
    VERSION_FROM => 'lib/license',
    ABSTRACT_FROM => 'lib/license.pod',
    LICENSE => 'unrestricted',
    PREREQ_PM => {
        'File::Basename' => '0',
        'JSON' => '0',
        'Path::Tiny' => '0',
        'DateTime' => '0',
        'File::HomeDir' => '0',
        'File::Spec' => '0'
    },
    TEST_REQUIRES => {
        'Test::More' => '0.47'
    },
    EXE_FILES => [
        'lib/license'
    ],
    META_MERGE => {
        'meta-spec' => {
            version => 2
        },
        resources => {
            repository => {
                type => 'hg',
                url => 'ssh://hg@bitbucket.org/intermaterium/license',
                web => 'https://bitbucket.org/intermaterium/license',
            },
        }
    },

);
